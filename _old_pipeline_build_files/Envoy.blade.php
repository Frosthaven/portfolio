@servers(['production' => 'dreamstate83@dragonscaleinteractive.com'])

@setup
    /* Project repository url */
    $repo = 'git@bitbucket.org:Frosthaven/portfolio.git';

    /* Project base directory as setup on the server*/
    $appDir = '/var/www/portfolio';

    /* repository branch we want to deploy*/
    $branch = 'main';

    date_default_timezone_set('UTC');

    /* we will use timestamp as directory name of
     ﻿every release */
    $date = date('YmdHis');

    /* directory where our release will be saved */
    $builds = $appDir . '/sources';

    /* full path to the release we are deploying */
    $deployment = $builds . '/' . $date;

    /* path to the symlink directory on the server */
    $serve = $appDir . '/current';

    /* path to our server .env file */
    $env = $appDir . '/.env';

    /* path to our storage directory */
    $storage = $appDir . '/storage';
@endsetup

@story('deploy')
    clone
    install
    live
    assets
    uncache
@endstory

@task('clone', ['on' => 'production'])
    git clone -b {{ $branch }} "{{ $repo }}" {{ $deployment }}
@endtask

@task('install', ['on' => 'production'])
    cd {{ $deployment }}
    rm -rf {{ $deployment }}/storage
    ln -nfs {{ $env }} {{ $deployment}}/.env
    ln -nfs {{ $storage }} {{ $deployment }}/storage
    composer install --prefer-dist
    php artisan migrate
@endtask

@task('live', ['on' => 'production'])
    {{--
        link the /current directory to the latest release in /sources

        rollback to previous versions with :
        ln -nfs /var/www/portfolio/sources/<release> /var/www/portfolio/current
    --}}
    ln -nfs {{ $deployment }} {{ $serve }}
@endtask

@task('assets',['on' => 'productionn'])
    npm install laravel-mix@latest
    npm install
    npm run production
@endtask

@task('uncache',['on' => 'production'])
    {{-- clear the app cache --}}
    php /var/www/portfolio/current/artisan cache:clear
    php /var/www/portfolio/current/artisan config:clear
    php /var/www/portfolio/current/artisan view:clear
    php /var/www/portfolio/current/artisan route:clear

    {{--
        clear the php fpm cache by reloading it. this requires sudo visudo

        (place at the end)

        www-data ALL = NOPASSWD: /usr/sbin/service php8.1-fpm reload
        dreamstate83 ALL = NOPASSWD: /usr/sbin/service php8.1-fpm reload
    --}}

    sudo /usr/sbin/service php8.1-fpm reload
    echo 'php8.1-fpm reloaded!'
@endtask
