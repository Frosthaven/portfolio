<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <link rel="manifest" href="{{ asset('/site.webmanifest') }}" />
    <link rel="icon" href="{{ asset('/favicon.ico?v=2') }}" />

    <title>Dragonscale Interactive</title>

</head>
<body>
    <div class="page-container">
        <section class="top dark">
            <img src="{{ asset('img/avatar-me.png') }}" alt="Shane Stanley Profile Picture" />
            <h1>Shane <span class="sub"><em>"Frosthaven"</em></span> Stanley</h1>
            <h2>Technological Problem Solver</h2>
            <div>
                <nav>
                    <a class="button" href="https://bitbucket.org/Frosthaven" target="_blank"><i class="fab fa-bitbucket" aria-hidden="true"></i><span>Bitbucket</span></a>
                    <a class="button" href="https://github.com/Frosthaven" target="_blank"><i class="fab fa-github" aria-hidden="true"></i><span>Github</span></a>
                    <a class="button" href="https://discordapp.com/users/141906786750955521" target="_blank"><i class="fab fa-discord" aria-hidden="true"></i><span>Discord</span></span></a>
                </nav>
                <p>
                    <span class="code">`Hello, <span class="code2">$(</span><span class="code3">world</span><span class="code2">)</span>!`</span>
                    <br /><br />
                    My name is Shane, and I'm a <strike>software engineer</strike> <strong>problem solver</strong>.
                    <br /><br />
                    The world of technology is full of ever-evolving dragons, and I'm looking for a team of armored warriors to tame them with. Below you will find listed some of my recent works.
                </p>
            </div>
        </section>
        <section class="project">
            <sub>Project</sub>
            <h1>
                Portfolio
            </h1>
            <div class="tech">
                <i class="fab fa-laravel"></i>
                <i class="fab fa-node"></i>
                <i class="fab fa-sass"></i>
            </div>
            <div class="project-flex">
                <div class="item image">
                    <div class="img">
                        <img src=" {{ asset('img/thumbnail/portfolio.png') }} "></img>
                    </div>
                </div>
                <div class="item">
                    <h2>Problem</h2>
                    <p>
                        A local organization reached out and asked me to apply after seeing my floating resume. I hadn't made my portfolio yet when they reached out, so I needed to conjure one up <i>and stat</i>.
                    </p>
                    <h2 class="solution">Solution</h2>
                    <p>
                        After looking up the organization's tech stack, I matched it with a new Laravel solution for my responsive portfolio and began digging into Laravel docs.
                    </p>
                    <h2 class="notes">Notes</h2>
                    <p>
                        Setting up automated zero downtime deployment by hand was a fun challenge. Lots of research and workarounds had to be done for several things such as clearing the opcache after deploy. It seems integrating modern front-ends such as react or vue has been thought of with Laravel, so I'll likely look into those options next.
                    </p>
                    <nav>
                        <a class="button" target="_blank" href="https://bitbucket.org/Frosthaven/portfolio"><i class="fab fa-bitbucket" aria-hidden="true"></i><span>Source Code</span></a>
                    </nav>
                </div>
            </div>
        </section>
        <section class="project">
            <sub>Project</sub>
            <h1>
                Voicemeeter Windows Volume
            </h1>
            <div class="tech">
                <i class="fab fa-node"></i>
            </div>
            <div class="project-flex">
                <div class="item image">
                    <div class="img">
                        <img src=" {{ asset('img/thumbnail/vmwv.png') }} "></img>
                    </div>
                </div>
                <div class="item">
                    <h2>Problem</h2>
                    <p>
                        Voicemeeter is a great piece of software that allows you to split audio between several different channels. The caveat, however, is that in doing so it is completely unmarried to the Windows volume UI. As a result, users cannot control volume with standard volume keys. They also have no popup indicating volume changes.
                        <br /><br />
                        Secondarily, I was in the market to research more about the power of webpack and strengthen my build toolchain.
                    </p>
                    <h2 class="solution">Solution</h2>
                    <p>
                        A javascript based tray applet deployed as an executable. This applet allows users to sync the Windows volume slider to any Voicemeeter strip they like (or several!). As a bonus, several driver issues can be worked around with this tool as well.
                    </p>
                    <h2 class="notes">Notes</h2>
                    <p>
                        Solving Windows-level problems with web technology is a bit esoteric, but it helped me strengthen build toolchain methodologies and made several users happy. Win / win! Early on in development one of the packages my code relied on (win-audio) was found to have massive memory leaks. To solve this I emulated what that package did in a hidden powershell worker window. The repository currently has a typescript fork, where I've been slowly migrating and upgrading over time.
                    </p>
                    <nav>
                        <a class="button" target="_blank" href="https://github.com/Frosthaven/voicemeeter-windows-volume"><i class="fab fa-github" aria-hidden="true"></i><span>Source Code</span></a>
                        <a class="button" target="_blank" href="https://github.com/Frosthaven/voicemeeter-windows-volume/releases/"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a>
                    </nav>
                </div>
            </div>
        </section>
        <section class="project">
            <sub>Practice</sub>
            <h1>
                Animated Card Flip
            </h1>
            <div class="tech">
                <i class="fab fa-node"></i>
                <i class="fab fa-react"></i>
            </div>
            <div class="project-flex">
                <div class="item image">
                    <div class="img">
                        <img src=" {{ asset('img/thumbnail/cardflip.png') }} "></img>
                    </div>
                </div>
                <div class="item">
                    <h2>Problem</h2>
                    <p>
                        It's been awhile since I've delved into web technologies at this point. I wanted to change that while using something relatively modern.
                    </p>
                    <h2 class="solution">Solution</h2>
                    <p>
                        A react based component with special effects. A two sided card that reacts to your mouse movement and can be flipped over completely (mobile friendly). This uses css transforms and javascript powered screen mathematics.
                    </p>
                    <h2 class="notes">Notes</h2>
                    <p>
                        This is prototype code.
                    </p>
                    <nav>
                        <a class="button" target="_blank" href="https://gitfront.io/r/Frosthaven/0f88d787da7c16202d112011daa8e2bedde3ee72/animated-card-flip/"><i class="fa fa-code" aria-hidden="true"></i><span>Source Code</span></a>
                        <a class="button" target="_blank" href="https://modest-mclean-955f9c.netlify.app/"><i class="fa fa-play-circle" aria-hidden="true"></i><span>Demo</span></a>
                    </nav>
                </div>
            </div>
        </section>
        <section class="project">
            <sub>Project</sub>
            <h1>
                Twitch Raid Campaign Reward Claimer
            </h1>
            <div class="tech">
                <i class="fab fa-python"></i>
                <i class="fab fa-edge"></i>
            </div>
            <div class="project-flex">
                <div class="item image">
                    <div class="img">
                        <img src=" {{ asset('img/thumbnail/twitchraid.png') }} "></img>
                    </div>
                </div>
                <div class="item">
                    <h2>Problem</h2>
                    <p>
                        Rare, the developers behind the game "Sea of Thieves" initiated a holiday event via Twitch. This event granted rewards, but required users to be online and interact to gain them. 70 hours is a long time to stare at a screen in a row.
                    </p>
                    <h2 class="solution">Solution</h2>
                    <p>
                        This is a python based tool that leverages selenium, a browser testing framework. By integrating a few browser plugins and intelligently automating Twitch website navigation, you could now gain the rewards in your sleep.
                    </p>
                    <h2 class="notes">Notes</h2>
                    <p>
                        One challenge that came up - Rare was using multiple activities. Sometimes it would be a raid you'd need to react to and sometimes it would be a host. When it was a host, users had to manually visit the hosted URL. This tool needed to handle both of these activities to be successful.
                    </p>
                    <nav>
                        <a class="button" target="_blank" href="https://github.com/Frosthaven/twitch-raid-campaign-reward-claimer"><i class="fab fa-github" aria-hidden="true"></i><span>Source Code</span></a>
                    </nav>
                </div>
            </div>
        </section>
        <section class="project">
            <sub>Project</sub>
            <h1>
                Deck Engine
            </h1>
            <div class="tech">
                <i class="fab fa-node"></i>
            </div>
            <div class="project-flex">
                <div class="item image">
                    <div class="img">
                        <img src=" {{ asset('img/thumbnail/deckengine.png') }} "></img>
                    </div>
                </div>
                <div class="item">
                    <h2>Problem</h2>
                    <p>
                        I picked up a streamdeck for personal use, but it wasn't as powerful as I wanted it to be.
                    </p>
                    <h2 class="solution">Solution</h2>
                    <p>
                        Using NodeJS, I created a new interface for the streamdeck. This interface supports pagination as well as uncapped potential through the Javascript language. Part of the initial development includes support for custom commands and autohotkey scripts. Controlling smart-bulbs from the streamdeck feels nice!
                    </p>
                    <h2 class="notes">Notes</h2>
                    <p>
                        Keeping node processes running and launched on boot can be a hassle, but thankfully PM2 exists for such a task. However, getting PM2 to automatically run on Windows is a rabbit itself. Rather than install services or other packages, I opted to manually run the resurrect command on user login.
                    </p>
                    <nav>
                        <a class="button" target="_blank" href="https://bitbucket.org/Frosthaven/deck-engine"><i class="fab fa-bitbucket" aria-hidden="true"></i><span>Source Code</span></a>
                    </nav>
                </div>
            </div>
        </section>
        <section class="project">
            <sub>Project</sub>
            <h1>
                Fortis Core Bot (auriga.io)
            </h1>
            <div class="tech">
                <i class="fab fa-node"></i>
                <i class="fab fab-discord"></i>
            </div>
            <div class="project-flex">
                <div class="item image">
                    <div class="img">
                        <img src=" {{ asset('img/thumbnail/fortisbot.png') }} "></img>
                    </div>
                </div>
                <div class="item">
                    <h2>Problem</h2>
                    <p>
                        The Fortis Core group were looking for a personal way to improve their Discord and Twitch presence integrations. They wanted a bot that could talk to both and be expanded to provide more features in the future.
                        <br /><br />
                        They would later request overlay technology that they could use with their streams.
                    </p>
                    <h2 class="solution">Solution</h2>
                    <p>
                        Class based NodeJS bot, running from a raspberry pi via PM2. This bot has a clean folder structure and an autoloader configured to pull commands, plugins, and services directly from the folders. By unifying the service class, all existing commands and plugins can execute across Twitch, Discord, or any future platform with ease.
                        <br /><br />
                        Later I would add Node Express into the mix with Socket.io - which would allow for per-user overlays to be configured and loaded into OBS or XSplit. You can see the overlay documentation <a target="_blank" href="https://docs.google.com/document/d/176nSFnQXeTqvUnos0duq1DWLeJG3F93zVw92y_aMt0c/edit?usp=sharing">here</a>, or give the custom twitch chat widget builder a view <a target="_blank" href="http://relay.fortiscoregaming.com:8080/theme-builder/theme-builder.html?bgcolor=rgba%2836%2C44%2C58%2C0.9%29&bordercolor=0x00ccff&namecolor=0xffffff&textcolor=0x0acff1&rotate=-13&hidepartial=true&autohide=true&bordergfx=http%3A%2F%2Frelay.fortiscoregaming.com%3A8080%2Fchat_css%2Fbl3.png%2C20%2C20%2C20%2Cstretch&hidebadges=false">with my Borderlands theme here</a>.
                    </p>
                    <h2 class="notes">Notes</h2>
                    <p>
                        The two most challenging parts in this project were creating a unified service layer for easily adding new platforms in the future, and creating a permission chain to tie it all together. They didn't want a database, and so I opted to use NEDB (a flat file storage that resembles MongoDB in practice).
                        <br /><br />
                        I'd like to add a special shout out to the myriad of APIs this bot uses. The ones that changed constantly you know who you are!
                    </p>
                    <nav>
                        <a class="button" target="_blank" href="https://bitbucket.org/fortiscore/fortisbot"><i class="fab fa-bitbucket" aria-hidden="true"></i><span>Source Code</span></a>
                    </nav>
                </div>
            </div>
        </section>
        <section class="project">
            <sub>Project</sub>
            <h1>
                Street Fighter V Pak Mod Manager
            </h1>
            <div class="tech">
                <i class="fab fa-node"></i>
                <i class="fa fa-atom"></i>
            </div>
            <div class="project-flex">
                <div class="item image">
                    <div class="img">
                        <img src=" {{ asset('img/thumbnail/pmm.gif') }} "></img>
                    </div>
                </div>
                <div class="item">
                    <h2>Problem</h2>
                    <p>
                        Street Fighter V supported modding with a few major drawbacks. The average user could not figure it out, as it required shuffling around system folders.
                        <br /><br />
                        Furthermore, DLC broke how mods worked. The game loaded content in the order of game > mods > dlc. What this meant was you could not modify anything that came from the dlc.
                        <br /><br >
                        Finally, I needed an excuse to try out electron.
                    </p>
                    <h2 class="solution">Solution</h2>
                    <p>
                        An electron based windows application. Mods could be distributed in pak files for easy consumption, with a completely drag and drop interface. This interface supported renaming packages and exporting.
                        <br /><br />
                        This got around the limitations of load order by tapping into the game's own DLC manifest JSON. By looking at the manifest and copying it, we could add our own pak files to that list and the load order would load them after the dlc.
                    </p>
                    <h2 class="notes">Notes</h2>
                    <p>
                        This had an attached squirrel auto-update server for awhile, but in the last released version I programmed the tool to adapt to changes in the manifest files automatically. This tool is still in use by thousands to this day.
                    </p>
                    <nav>
                        <a class="button" target="_blank" href="https://bitbucket.org/Frosthaven/sfv-mod-manager"><i class="fab fa-bitbucket" aria-hidden="true"></i><span>Source Code</span></a>
                        <a class="button" target="_blank" href="https://mega.nz/#!wZJGCQqZ!oajiyPmmWXrPpePcqgJiL9BJTAcHqTiOiOCtGZ1TMcw"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a>
                    </nav>
                </div>
            </div>
        </section>
        <section class="project">
            <sub>Project</sub>
            <h1>
                Fortis Core Web Property
            </h1>
            <div class="tech">
                <i class="fab fa-symfony"></i>
                <i class="fab fa-node"></i>
                <i class="fab fa-sass"></i>
                <i class="fa fa-database"></i>
            </div>
            <div class="project-flex">
                <div class="item image">
                    <div class="img">
                        <img src=" {{ asset('img/thumbnail/fortisweb.png') }} "></img>
                    </div>
                </div>
                <div class="item">
                    <h2>Problem</h2>
                    <p>
                        I was asked to create a web property for the Fortis Core gaming group. They required a responsive website with a light cms to manage articles related to gaming. They also had a podcast at the time, and needed a way to link podcasts to articles.
                    </p>
                    <h2 class="solution">Solution</h2>
                    <p>
                        Symfony project. The web application had the ability to share content directly to social media sites (with appropriate thumbnails), and users could create new articles where everything was parsed via markdown.
                        <br /><br />
                        The markdown syntax was further expanded to allow for image carousels and other assorted article format additions. Users could post a podcast article to play directly from soundcloud. The Symfony backend also supported RSS feed for all articles and podcasts.
                    </p>
                    <h2 class="notes">Notes</h2>
                    <p>
                        One fun challenge in this application was getting infinite page scrolling AND pagination working as options. This was also around the time that the browser history api was starting to see itself in the market. Leveraging that api allowed the user to dynamically load articles as a modal when they clicked on them in navigation - complete with proper SEO and url change with dated and slugged article urls.
                        <br /><br />
                        This web property is no longer in operation.
                    </p>
                    <nav>
                        <a class="button" target="_blank" href="https://bitbucket.org/fortiscore/fortis-web/"><i class="fab fa-bitbucket" aria-hidden="true"></i><span>Source Code</span></a>
                    </nav>
                </div>
            </div>
        </section>
        <section class="project">
            <sub>Other</sub>
            <h1>
                Made From Scratch Keyboard
            </h1>
            <div class="tech">
                <i class="fa fa-tools"></i>
            </div>
            <div class="project-flex">
                <div class="item image">
                    <div class="img">
                        <img src=" https://i.imgur.com/IFrSuDs.jpg"></img>
                    </div>
                </div>
                <div class="item">
                    <h2>Problem</h2>
                    <p>
                        This isn't so much a coding project, but rather a peak at one of my other hobbies. If I'm not coding, I'm either gaming or doing things like this!
                        <br /><br />
                        I wanted something a bit more ergonomic than standard rowstagger keyboards, and I wanted something wireless and programmable.
                        <br /><br />
                        This is a custom 40% keyboard with low profile keys, built from the ground up.
                    </p>
                    <h2 class="solution">Solution</h2>
                    <p>
                        I wanted something a bit more ergonomic than standard row stagger keyboards, and I wanted something wireless and programmable.
                    </p>
                    <h2 class="notes">Notes</h2>
                    <p>
                        I'm not a C# guy, but customizing this to my liking did require forking the nice!nano microcontroller firmware. I've defined a custom rgb set and layout, and pulled in work from other contributers that had not hit upstream yet. You'll find links to those repositories below.
                    </p>
                    <nav>
                        <a class="button" target="_blank" href="https://imgur.com/a/8zQm073"><i class="fa fa-images" aria-hidden="true"></i><span>Build Log</span></a>
                        <a class="button" target="_blank" href="https://github.com/Frosthaven/zmk"><i class="fab fa-github" aria-hidden="true"></i><span>Firmware Source Code</span></a>
                        <a class="button" target="_blank" href="https://github.com/Frosthaven/zmk-config"><i class="fab fa-github" aria-hidden="true"></i><span>Config Source Code</span></a>
                    </nav>
                </div>
            </div>
        </section>
    </div>
</body>
</html>
