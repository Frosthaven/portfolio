# Welcome

This is my personal portfolio. I had to rush to gather it together for a meeting, and so upgrades are pending.

# Projects To Include (done)

-   this portfolio (bitbucket)
-   voicemeeter-windows-volume (github)
-   animated card flip
-   twitch-raid-campaign-reward-claimer (github)
-   deck-engine
-   sfv-mod-manager (bitbucket)
-   fortis-web (bitbucket)
-   fortisbot (bitbucket)
    -web overlay tech

# Todo

-   Add deployer deploy chain
-   Implement Twill or custom CSS
-   More presentable imagery / css effects
-   Add contact box
-   Add return to top widget
-   Convert to React or Vue front-end
