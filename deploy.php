<?php
namespace Deployer;

require 'recipe/laravel.php';

// Notes:
//  This requires a linux user with the following setup:
//      administrative rights
//          usermod -aG sudo <username>
//      sudo visudo
//          <username> ALL = NOPASSWD: /usr/sbin/service php* reload
//          <username> ALL = NOPASSWD: /usr/bin/setfacl */releases
//          <username> ALL = NOPASSWD: /usr/bin/rm -rf node_modules


// EASY CONFIG *****************************************************************
$config = array(
    "app"  => "portfolio",
    "user"         => "deployer",
    "host"         => "dragonscaleinteractive.com",
    "repo"         => "https://bitbucket.org/Frosthaven/portfolio/",
    "path"         => "/var/www/{{ application }}",
);

// DEPLOYER SETUP **************************************************************
set('application', $config['app']);
set('repository', $config['repo']);
set('git_tty', true);
set('allow_anonymous_stats', false);


add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);

host($config['user'] . "@" . $config['host'])
    ->set('remote_user',$config['user'])
    ->set('deploy_path', $config['path']);

// TASKS ***********************************************************************
task('cleanup:permissions', function() {
    run("sudo setfacl -R -m u:deployer:rwx {{deploy_path}}/releases");
});

task('build:assets', function() {
    run("cd {{release_path}} && npm install");
    run("cd {{release_path}} && npm run prod");
    run("cd {{release_path}} && sudo rm -rf node_modules");
});

task('deploy:uncache', function () {
    run("cd {{release_path}} && php artisan cache:clear");
    run("cd {{release_path}} && php artisan config:clear");
    run("cd {{release_path}} && php artisan view:clear");
    run("cd {{release_path}} && php artisan route:clear");
    run("sudo /usr/sbin/service php8.1-fpm reload");
});

// auto unlock
after('deploy:writable', 'build:assets');
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');

// extra requirements
after('deploy:unlock', 'deploy:uncache');
before('cleanup','cleanup:permissions');
